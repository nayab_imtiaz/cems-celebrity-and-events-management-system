"""Evaluation URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from CEMS import views as views
urlpatterns = [
    url(r'^admin/',admin.site.urls),
    url(r'^login/$', auth_views.login, {'template_name': 'CEMS/login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout,{'next_page': auth_views.login}, name='logout'),
    url(r'^welcome/$', views.welcome, name='welcome'),
    url(r'^addcelebrity/$', views.addcelebrity, name='addcelebrity'),
    url(r'^addevent/$', views.addevent, name='addevent'),
    url(r'^viewevent/$', views.viewevent, name='viewevent'),
    url(r'^allceleb/$', views.allceleb, name='viewceleb'),
    url(r'^showcelebrities/(?P<event_id>[0-9]+)/$$', views.showcelebrities, name='showcelebrity'),

    url('api/', include('CEMS.urls'))
]
