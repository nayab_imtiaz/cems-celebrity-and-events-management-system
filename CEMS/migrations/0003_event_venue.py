# -*- coding: utf-8 -*-
# Generated by Django 1.11.15 on 2018-09-10 13:19
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CEMS', '0002_auto_20180910_1043'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='venue',
            field=models.CharField(max_length=100, null=True),
        ),
    ]
