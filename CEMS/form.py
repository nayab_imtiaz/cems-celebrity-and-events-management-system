from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import ModelForm
from models import Celebritiy, Event

class CelebrityClass(forms.ModelForm):


    class Meta:
        model = Celebritiy
        fields = ('name', 'age', 'net_worth', 'email','dob', )


class EventClass(forms.ModelForm):


    class Meta:
        model = Event
        fields = ('event_name', 'venue', 'event_date', 'celebrities')


