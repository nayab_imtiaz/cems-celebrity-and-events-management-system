from django.db import models
from  django.contrib.auth.models import User


class Celebritiy(models.Model):
    name=models.CharField(max_length=30)
    age=models.IntegerField()
    net_worth = models.IntegerField()
    email=models.EmailField(max_length=300)
    dob=models.DateField()

    def __str__(self):
        return self.name



class Event(models.Model):
    event_name=models.CharField(max_length=30)
    venue=models.CharField(max_length=100, null= True)
    event_date = models.CharField(max_length=100)
    celebrities = models.ManyToManyField(Celebritiy,null=True)

    def __str__(self):
        return self.event_name