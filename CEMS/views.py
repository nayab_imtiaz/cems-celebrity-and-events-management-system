# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from models import Event, Celebritiy
from django.shortcuts import render, redirect
from CEMS.form import CelebrityClass , EventClass
# Create your views here.
from django.contrib.auth.decorators import login_required
from rest_framework import viewsets
from .serializer import celebritySerializer, eventSerializer

class celebrityView(viewsets.ModelViewSet):
    queryset = Celebritiy.objects.all()
    serializer_class = celebritySerializer



class eventView(viewsets.ModelViewSet):
    queryset = Event.objects.all()
    serializer_class = eventSerializer

@login_required
def showadminpanel(request):

        return render(request, 'CEMS/adminpanel.html')

@login_required
def welcome(request):

    return  redirect('viewevent')

    #return render(request,'CEMS/adminpanel.html')

@login_required
def addcelebrity(request):

        if request.method == 'POST':
            form = CelebrityClass(request.POST)

            if form.is_valid():
             form.save()
             return redirect('welcome')


        else:
            context = {

                'form_name': 'add_form'
            }
            form = CelebrityClass()
        return render(request, 'CEMS/addcelebrity.html', {'form': form})

@login_required
def editcelebrity(request, id):

        celeb = Celebritiy.objects.filter(pk=id)
        if request.method == 'POST':

            form = CelebrityClass(request.POST, instance=celeb)

            if form.is_valid():

                form.save()
                return redirect('welcome')

        else:

           form = CelebrityClass(instance=celeb)
           context= {

               'form_name': 'edit_form'
           }
           return render(request, 'CEMS/addcelebrity.html', {'form': form})

@login_required
def addevent(request):

        if request.method == 'POST':
            form = EventClass(request.POST)

            if form.is_valid():
                event= form.save()
                selected_celebrities= form.cleaned_data['celebrities']
                for celeb in selected_celebrities:
                        event.celebrities.add(celeb)


                return redirect('welcome')


        else:
            context = {

                'form_name': 'add_form'
            }
            form = EventClass()
        return render(request, 'CEMS/addevent.html', {'form': form})

@login_required
def viewevent(request):

        all_events = Event.objects.all()
        return render(request, 'CEMS/viewevent.html', {'events': all_events})

@login_required
def showcelebrities(request,event_id):

        event = Event.objects.get(pk=event_id)
        print (event)
        celeb= event.celebrities.all()
        print (celeb)
        return render(request, 'CEMS/showceleb.html', {'celebrities': celeb})

@login_required
def allceleb(request):

        celeb=Celebritiy.objects.all()
        return render(request, 'CEMS/showceleb.html', {'celebrities': celeb})

