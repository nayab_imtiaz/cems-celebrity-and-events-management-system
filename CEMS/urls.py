from django.conf.urls import url, include
from .import views
from rest_framework import routers


router= routers.DefaultRouter()
router.register('viewevents', views.eventView)
router.register('viewceleb', views.celebrityView)

urlpatterns = [
    url('', include(router.urls)),
        url(r'^panel/', views.showadminpanel, name='panel'),


    ]


